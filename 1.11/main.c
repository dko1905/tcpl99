#include <stdio.h>
/* === 1.11 ===
 * Testing/breaking the word count program.
 */

#define IN 1
#define OUT 0

int main(){
	int c, nl, nw, nc, state;

	state = OUT;
	nl = nw = nc = 0;

	while ((c = getchar()) != EOF) {
		++nc;
		if (c == ' ' || c == '\n' || c == '\t') {
			if (c == '\n') {
				++nl;
			}
			state = OUT;
		} else if (state == OUT) {
			state = IN;
			++nw;
		}
	}
	printf("lines: %i words: %i chars: %i\n", nl, nw, nc);

	return 0;
}
