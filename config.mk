# Flags
MYCPPFLAGS = -D_POSIX_C_SOURCE=200112L # My Preprocessor
MYCFLAGS = -std=c99 -Wall -Wextra -pedantic \
           $(MYCPPFLAGS) $(CPPFLAGS) $(CFLAGS) # My C-flags
MYLDFLAGS = $(LDFLAGS) # My LD flags
