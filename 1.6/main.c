#include <stdio.h>
/* === 1.6 ===
 * What is EOF?
 */

int main() {
	int c = getchar() != EOF;
	printf("getchar() != EOF is equal to %d\n", c);

	return 0;
}
