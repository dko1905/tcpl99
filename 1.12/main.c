#include <stdio.h>
/* === 1.12 ===
 * Print one word per line.
 */

int main(){
	int c;

	while ((c = getchar()) != EOF) {
		if (c == ' ' || c == '\n' || c == '\t') {
			printf("\n");
		} else {
			printf("%c", c);
		}
	}

	return 0;
}
