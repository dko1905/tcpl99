#include <stdio.h>
/* === 1.8 ===
 * Counting spaces, tabs and lines.
 */

int main() {
	int c, spaces = 0, tabs = 0, newlines = 0;

	while ((c = getchar()) != EOF) {
		if (c == ' ') {
			++spaces;
		} else if (c == '\t') {
			++tabs;
		} else if (c == '\n') {
			++newlines;
		}
	}

	printf("%i spaces, %i tabs and %i newlines.\n", spaces, tabs, newlines);

	return 0;
}
