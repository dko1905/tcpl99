#include <stdio.h>
/* === 1.10 ===
 * Escape tabs, backspaces and newlines.
 */

int main() {
	int c;

	while ((c = getchar()) != EOF) {
		if (c == '\t') {
			printf("\\t");
		} else if (c == '\b') {
			printf("\\b");
		} else if (c == '\n') {
			printf("\\n");
		} else {
			printf("%c", c);
		}
	}

	return 0;
}
