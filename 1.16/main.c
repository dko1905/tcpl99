#include <stdio.h>
#include <stdlib.h>
/* === 1.16 ===
 * Longest line finder (with accurate length).
 */
#define MAXLINE 25

/* Read line from stdin, will auto zeroterminate. */
int getline2(char s[], int lim);
/* Copy byte by byte from src to dest. */
void copy(char *dest, char *src);

int main(){
	char line[MAXLINE] = {0}, maxLine[MAXLINE] = {0};
	int len, maxLen = 0;

	while ((len = getline2(line, MAXLINE)) != EOF) {
		if (len > maxLen) {
			maxLen = len;
			copy(maxLine, line);
		}
	}

	printf("Longest line was %i chars. %i char snippet:\n%s\n", maxLen, MAXLINE, maxLine);

	return 0;
}

int getline2(char s[], int lim) {
	int c, i = 0;

	while ((c = getchar()) != EOF && i < (lim - 1) && c != '\n') {
		s[i++] = c;
	}
	s[i] = '\0';
	while (c != EOF && c != '\n') {
		++i;
		c = getchar();
	}

	if (i == 0 && c == EOF) return EOF;
	return i;
}

void copy(char *dest, char *src) {
	while (*src != '\0')
		*(dest++) = *(src++);
	*dest = '\0';
}
