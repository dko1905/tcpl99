#include <stdio.h>
#include <stdlib.h>
/* === 1.14 ===
 * ASCII char histogram.
 */
#define IN 0
#define OUT 1

/* Escape special chars. */
char *escape(char c);

int main() {
	/* We support all ASCII charectors, there are exactly 128 different
	 * chars. Because of that we can just use the char as the index in the arr.
	 */
	int freq[128] = {0};
	int c;

	/* Count chars. */
	while ((c = getchar()) != EOF) {
		if ((unsigned char)c < 128) {
			++freq[(unsigned char)c];
		}
	}
	/* Print histogram. */
	for (int n = 0; n < 128; ++n) {
		if (freq[n] < 1) continue;
		printf("\"%s\": ", escape(n));
		for (int i = freq[n]; i > 0; --i) {
			printf("#");
		}
		printf("\n");
	}

	return 0;
}

/* Buffer for when passing input c out. */
static char cbuf[2] = {0};
char* escape(char c) {
	switch (c) {
		case '\n':
			return "\\n";
		case '\t':
			return "\\t";
		default:
			/* We could take the address of c, because it's an int, but
			 * that's bad code. */
			cbuf[0] = c;
			return cbuf;
	}
}
