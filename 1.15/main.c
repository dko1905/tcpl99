#include <stdio.h>
#include <stdlib.h>
/* === 1.15 ===
 * Celsius to Fahr rewritten with a function.
 */
float fahr_to_celsius(float fahr);

int main(){
	int lower, upper, step;

	lower = 0;
	upper = 100;
	step = 10;

	printf("Fahr | Celsius\n");
	for(float fahr = lower; fahr <= upper; fahr += step){
		printf("%3.0f %6.1f\n", fahr, fahr_to_celsius(fahr));
	}

	return 0;
}

float fahr_to_celsius(float fahr){
	return (5.0/9.0) * (fahr - 32.0);
}