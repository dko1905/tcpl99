# THE C PROGRAMMING LANGUAGE (SECOND EDITION)
Here are my personal answers to **all** exercises **with tests**.
I am using C99 with POSIX.1-2001 to complete the exercises.

## Compiling & running
Run the `Makefile` generator script:
```sh
./genmake > Makefile
```
You can now run:
```sh
make help
```
to get possible targets.

## License
[GPL-3](./LICENSE)

