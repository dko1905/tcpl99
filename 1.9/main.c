#include <stdio.h>
/* === 1.9 ===
 * Remove duplicate blanks.
 */

int main() {
	int c, last = 0;

	while ((c = getchar()) != EOF) {
		if (c != ' ') {
			if (last == ' ') {
				printf(" ");
			}
			printf("%c", c);
		}
		last = c;
	}

	return 0;
}
